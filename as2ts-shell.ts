
declare var saveAs;

module as2ts
{
    export function main()
    {
       
       new as2ts();
    }

    

    export class as2ts
    {

        
        public constructor()
        {
            var fs = require("fs");
            var argv = require( 'argv' );
            var options = {
                name: 'output',
                short: 'o',
                type: 'string',
                description: 'Defines an output filename for script',
                example: "'script --output=file.ts file.as'  or 'script -o file.ts file.as'"
            }
            var args = argv.option( options ).run();
            //console.log(args);
            // Setup the dnd listeners.
            var filenames = args.targets[0];
            var outptufilename = args.options.output;
            
            if(filenames && outptufilename) {
                if( fs.existsSync( filenames ) ) {
                    var inputstr =  fs.readFileSync(filenames,'utf-8'); 
                    var outputstr = this.convert(inputstr);
                    fs.writeFileSync(outptufilename, outputstr);
                }
                else {
                    console.error(`file not found ${filenames}`);
                }
                
            }
            else {
                argv.help();
            }
             
        }

        private convert (str:string):string
        {

            //get all static variables
            //console.log(str.match(/(public|private|protected|internal)\s+static\s+var\s+(\w+)/g));

            //  Boolean to boolean
            str = str.replace(/\bBoolean\b/g,"boolean");
            //  uint/int/Number to number
            str = str.replace(/:\b(int|uint|Number)\b/g,":number");
            //  String to string
            str = str.replace(/\bString\b/g,"string");
            //  * to any
            str = str.replace(/:\s*\*/g,":any");


            //  'package' to 'module'
            str = str.replace(/package/,"module");
            //  comment out import statements
            str = str.replace(/import/g,"//import"); //?   /// <reference path="Validation.ts" />
            //  'public class' to 'export class'
            //  'public final class' to 'export class'
            str = str.replace(/public\s+(final\s+)?class/,"export class");
            //  'public interface' to 'export interface'
            str = str.replace(/public\s+interface/g,"export interface");

            // constructor
            var classNameResult = /export.class\s+(\w+)/.exec(str);
            if(classNameResult != null)
            {
                var className = classNameResult[1];
                var reg = new RegExp("public\\s+function\\s+"+className);
                str = str.replace(reg,"constructor");
                str = str.replace(/(constructor.+):.*void/,"$1")//delete :void
            }
 


            //  'internal' to 'public'
            str = str.replace(/\binternal\b/g,"public");
            //  swap static order
            str = str.replace(/static\s+(public|private|protected)/g,"$1 static");

            //   'public var'                   to 'public'
            //   'public const'                 to 'public'
            //   '(override) public function'   to 'public'
            str = str.replace(/(override\s+)?(private|public|protected)\s+(var|const|function)/g,"$2");

            // 'public static var'       to  'public static'
            // 'public static const'     to  'public static'
            // 'public static function'  to  'public static'
            str = str.replace(/(public|private|protected)\s+static\s+(var|const|function)/g,"$1 static");

            // local const to var
            str = str.replace(/\bconst\b/g,"var");


            //  'trace' to 'console.log'
            str = str.replace(/trace\s*\(/g,"console.log(");
            //  'A as B' to '<B> A'
            str = str.replace(/(\w+)\s+(\bas\b)\s+(\w+)/g,"<$3> $1");

            //  ':Array' to 'any[]'
            str = str.replace(/:\s*Array/g,":any[]");

            //  ':Vector.<number> =' to 'number[] ='
            str = str.replace(/:\s*Vector\.<(.+)>\s*=/g,":$1[] =");
            //  ':Vector.<number>;' to 'number[];'
            str = str.replace(/:\s*Vector\.<(.+)>\s*;/g,":$1[] ;");
            // ': Vector.<string> {' to 'string[] {'
            str = str.replace(/:\s*Vector\.<(.+)>\s*{/g,":$1[] {");

            //  'new Vector.<uint>(7,true)' to '[]'
            str = str.replace(/new\s+Vector\.<.+>(\(.+\))?/g,"[]");
            //  'new <uint>[1,2,3]'   to  '[1,2,3]'
            str = str.replace(/new\s+<.+>(\[.*\])/g,"$1");
            //  'Vector.<uint>([1, 2, 3])' to '[1, 2, 3]'
            str = str.replace(/(=|\s)Vector\.<.+>\((\[.*\])\)/g,"$2");

            //TODO: add 'this.' to all instance methods
            //TODO: add 'className.' to all static methods
            return str;

        }




    }


}
as2ts.main();